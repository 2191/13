# Сумма членов ряда последовательности
obb = 42
file = open(r"C:\Users\Администратор\PycharmProjects\pythonProject15\file11.txt") # Открытие файла

# Сложение файловых строк
while True:
    line = file.readline()  # Получение строки
    if not line:
        break
    obb += 1 / int(line)  # Вычисление данных
file.close()
print(f'Сумма ряда: {obb}')
